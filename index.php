<?php 
    
require_once 'lib/db.config.php';
require_once 'lib/database.class.php';

// A function to print the values to debug
function die_r($value){
    echo '<pre>';
    print_r($value);
    echo '</pre>';
}

$dbh = new Database();



?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>
    <!-- Bootstrap -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    <div class="container">
        <div class="row">
            <h3>PHP CRUD Grid</h3>
        </div>
        <div class="row">
            <p><a class="btn btn-xs btn-success" href="create.php">Create</a></p>
            <table class="table table-striped table-bordered table-hover">
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Age</th>
                    <th>Gender</th>
                    <th>Action</th>
                </tr>
                <tbody>
                 <?php 
                 
                    $sql = "SELECT * FROM tb_crud";
                    $result = $dbh->getRows($sql);
                    foreach ($result as $row) {
                        ?>
                    <tr>
                      <td><?php echo $row['id'] ?></td>
                      <td><?php echo $row['fname'].' '.$row['fname'] ?></td>
                      <td><?php echo $row['age'] ?></td>
                      <td><?php echo $row['gender'] ?></td>
                      <td>
                        <a class="btn btn-xs btn-info" href="read.php?id=<?php echo $row['id']; ?>">Read</a>
                        <a class="btn btn-xs btn-primary" href="#">Update</a>
                        <a class="btn btn-xs btn-danger" href="#">Delete</a>
                      </td>
                    </tr>                    
                    <?php
                    }
                 
                 ?>             
                </tbody>
            </table>
        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
</body>

</html>
