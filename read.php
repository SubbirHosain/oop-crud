<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>
    <!-- Bootstrap -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    <div class="container">
        <div class="col-sm-12">
            <div class="row">
                <h3>Read a User</h3>
            </div>
            <div class="form-group col-sm-12">
                <label class="col-sm-2 control-label">First Name</label>
                <div class="col-sm-10">
                    <p class="form-control-static">IQtidar</p>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <label class="col-sm-2 control-label">Last Name</label>
                <div class="col-sm-10">
                    <p class="form-control-static">Shah</p>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <label class="col-sm-2 control-label">Age</label>
                <div class="col-sm-10">
                    <p class="form-control-static">31</p>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <label class="col-sm-2 control-label">Gender</label>
                <div class="col-sm-10">
                    <p class="form-control-static">male</p>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <a class="btn btn btn-default" href="index.html">Back</a>
            </div>
        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
</body>

</html>
