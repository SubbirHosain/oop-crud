<?php 
    require_once 'lib/db.config.php';
    require_once 'lib/database.class.php';
    $dbh = new Database();
    if(isset($_POST['insert_data']))
    {   
        extract($_POST);
        //add validation here according to your requirements
        $valid = TRUE;
        if(empty($fname))
        {
            echo "First name required";
           $valid = FALSE;
        }
        
        //after all validation insert data
        if($valid)
        {
            $insert_sql = "INSERT INTO `tb_crud` (fname,lname,age,gender) VALUES(?,?,?,?)";
            $data = array($fname,$lname,$age,$gender);
            $msg = $dbh->insertRow($insert_sql, $data);
            if($msg)
            {
                echo "Data inserted";
            }
        }
    }

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>
    <!-- Bootstrap -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    <div class="container">
        <div class="row">
            <h3>Create a User</h3>
        </div>
        <div class="row">
            <form method="POST" action="">
                <div class="form-group ">
                    <label for="inputFName">First Name</label>
                    <input type="text" class="form-control"  id="inputFName" value="" name="fname" placeholder="First Name">
                    <span class="help-block"></span>
                </div>
                <div class="form-group ">
                    <label for="inputLName">Last Name</label>
                    <input type="text" class="form-control"  id="inputLName" value="" name="lname" placeholder="Last Name">
                    <span class="help-block"></span>
                </div>
                <div class="form-group ">
                    <label for="inputAge">Age</label>
                    <input type="number"  class="form-control" id="inputAge" value="" name="age" placeholder="Age">
                    <span class="help-block"></span>
                </div>
                <div class="form-group ">
                    <label for="inputGender">Gender</label>
                    <select class="form-control"  id="inputGender" name="gender">
                        <option></option>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                    </select>
                    <span class="help-block"></span>
                </div>
                <div class="form-actions">
                    <button type="submit" name="insert_data" class="btn btn-success">Create</button>
                    <a class="btn btn btn-default" href="index.php">Back</a>
                </div>
            </form>
        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
</body>

</html>
