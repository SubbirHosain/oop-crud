<?php

class Database {

    private $isConn;
    private $db_name = DB_NAME;
    private $db_user = DB_USER;
    private $db_pass = DB_PASS;
    private $db_host = DB_HOST;
    private $dbh;
    //little modification
    /* private $options  = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,); */
    public function __construct() {
        if (!isset($this->dbh)) {
            //set data source name
            $dsn = 'mysql:host=' . $this->db_host . ';dbname=' . $this->db_name;
            //set Custom PDO options.
            $options = array(PDO::ATTR_PERSISTENT => true);
            // Create a new PDO instanace
            try {
                $link = new PDO($dsn, $this->db_user, $this->db_pass, $options);
                $link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                //set the default set mode here
                //$link->setFetchMode(PDO::FETCH_ASSOC);
                $this->dbh = $link;
                $this->isConn = TRUE;
            } catch (PDOException $exc) {
                echo "Failed to connect with database" . $e->getMessage();
            }
        }
    }

    // disconnect from db
    public function Disconnect() {
        $this->dbh = NULL;
        $this->isConn = FALSE;
    }

    //get single row
    public function getRow($query, $params = []) {
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute($params);
            return $sth->fetch();
        } catch (PDOException $exc) {
            echo 'Error' . $exc->getMessage();
        }
    }

    // get multiple rows
    public function getRows($query, $params = []) {
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute($params);
            return $sth->fetchAll();
        } catch (PDOException $exc) {
            echo 'Error' . $exc->getMessage();
        }
    }

    // insert row
    public function insertRow($query, $params = []) {

        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute($params);
            return TRUE;
        } catch (PDOException $exc) {
            echo 'Error' . $exc->getMessage();
        }
    }
    // update row
    public function updateRow($query, $params = []){
        $this->insertRow($query, $params);
    }
    // delete row
    public function deleteRow($query, $params = []){
        $this->insertRow($query, $params);
    }    
}
