<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>
    <!-- Bootstrap -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="row">
                <h3>Update a User</h3>
            </div>
            <form method="POST" action="update.php?id=2436">
                <div class="form-group ">
                    <label for="inputFName">First Name</label>
                    <input type="text" class="form-control" required="required" id="inputFName" value="Subbir" name="fname" placeholder="First Name">
                    <span class="help-block"></span>
                </div>
                <div class="form-group ">
                    <label for="inputLName">Last Name</label>
                    <input type="text" class="form-control" required="required" id="inputLName" value="Hossain" name="lname" placeholder="Last Name">
                    <span class="help-block"></span>
                </div>
                <div class="form-group ">
                    <label for="inputAge">Age</label>
                    <input type="number" required="required" class="form-control" id="inputAge" value="22" name="age" placeholder="Age">
                    <span class="help-block"></span>
                </div>
                <div class="form-group ">
                    <label for="inputGender">Gender</label>
                    <select class="form-control" required="required" id="inputGender" name="gender">
                        <option></option>
                        <option value="male" selected>Male</option>
                        <option value="female">Female</option>
                    </select>
                    <span class="help-block"></span>
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn btn-primary">Update</button>
                    <a class="btn btn btn-default" href="index.html">Back</a>
                </div>
            </form>
        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
</body>

</html>
